package searchengine.model;

import jdk.jfr.Name;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class Site {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('INDEXING', 'INDEXED', 'FAILED') NOT NULL")
    private StatusType status;

    @Name("status_time")
    @Column(columnDefinition = "DATETIME NOT NULL")
    private Date statusTime;

    @Name("last_error")
    @Column(columnDefinition = "TEXT")
    private String lastError;

    @Column(columnDefinition = "VARCHAR(255) NOT NULL")
    private String url;

    @Column(columnDefinition = "VARCHAR(255) NOT NULL")
    private String name;

    @OneToMany(mappedBy = "siteId")
    private Set<Page> pageSet;

    @OneToMany(mappedBy = "siteId")
    private Set<Lemma> lemmaSet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public Date getStatusTime() {
        return statusTime;
    }

    public void setStatusTime(Date statusTime) {
        this.statusTime = statusTime;
    }

    public String getLastError() {
        return lastError;
    }

    public void setLastError(String lastError) {
        this.lastError = lastError;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Page> getPageSet() {
        return pageSet;
    }

    public void setPageSet(Set<Page> pageSet) {
        this.pageSet = pageSet;
    }

    public Set<Lemma> getLemmaSet() {
        return lemmaSet;
    }

    public void setLemmaSet(Set<Lemma> lemmaSet) {
        this.lemmaSet = lemmaSet;
    }
}
