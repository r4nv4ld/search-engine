package searchengine.dto.search;

import lombok.Data;

import java.util.List;

@Data
public class SearchResponse {
    private Boolean result;
    private String error;
    private String query;
    private Integer count;
    private List<DataSearchResponse> data;
}
