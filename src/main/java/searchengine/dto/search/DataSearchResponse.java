package searchengine.dto.search;

import lombok.Data;

@Data
public class DataSearchResponse {
    private Integer site;
    private String siteName;
    private String url;
    private String title;
    private String snippet;
    private String relevance;
}
