package searchengine.dto.indexing;

import lombok.Data;

@Data
public class IndexPageResponse {
    private Boolean result;
    private String error;
}
