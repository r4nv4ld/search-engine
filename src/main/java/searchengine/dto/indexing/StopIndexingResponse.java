package searchengine.dto.indexing;

import lombok.Data;

@Data
public class StopIndexingResponse {
    private Boolean result;
    private String error;
}
