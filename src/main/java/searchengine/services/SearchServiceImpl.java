package searchengine.services;

import lombok.RequiredArgsConstructor;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;
import searchengine.dto.search.DataSearchResponse;
import searchengine.dto.search.SearchResponse;
import searchengine.model.Index;
import searchengine.model.Lemma;
import searchengine.model.Page;
import searchengine.repositorys.IndexRepository;
import searchengine.repositorys.PageRepository;
import searchengine.services.indexingsite.LemmaFinder;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final PageRepository pageRepository;
    private final IndexRepository indexRepository;
    private HashMap<String, String> lemmaWordMap;

    public SearchResponse getSearch(String query, String site, int offset, int limit) {

        SearchResponse searchResponse = new SearchResponse();

        try {
            List<Page> searchPageList = new ArrayList<>();
            HashSet<Page> generalPages;
            List<Index> searchIndex = new ArrayList<>();
            HashSet<Lemma> searchLemma = new HashSet<>();
            Map<Page, Double> relevanceRelative;
            Integer limitCount = limit;
            Integer offsetCount = offset;

            if (query.length() == 0) {
                searchResponse.setResult(false);
                searchResponse.setError("Задан пустой поисковый запрос");
                return searchResponse;
            }

            setSearchPageList(site, searchPageList);
            setSearchIndex(query, searchIndex, searchLemma);
            generalPages = sortedPages(searchLemma, searchIndex, searchPageList);
            relevanceRelative = relevance(generalPages, searchIndex);
            searchResponse = setResponse(searchResponse, query, relevanceRelative, limitCount, offsetCount);


        } catch (Exception e) {
            System.out.println(e);
        }

        return searchResponse;
    }

    private SearchResponse setResponse(SearchResponse searchResponse, String query,
                                       Map<Page, Double> relevanceRelative,
                                       Integer limitCount, Integer offsetCount) throws IOException {

        searchResponse.setResult(true);
        searchResponse.setQuery(query);
        searchResponse.setCount(relevanceRelative.size());

        List<DataSearchResponse> data = new ArrayList<>();

        int counter = 0;

        for (Page page : relevanceRelative.keySet()) {
            if (data.size() == limitCount) {
                break;
            }
            DataSearchResponse item = new DataSearchResponse();
            String title = Jsoup.connect(page.getPath()).get().title();

            counter++;

            item.setSite(page.getSiteId().getId());
            item.setSiteName(page.getSiteId().getName());
            item.setUrl(page.getPath());
            item.setTitle(title);
            item.setSnippet(snippet(page.getContent(), query));
            item.setRelevance(String.valueOf(relevanceRelative.get(page)));

            if (counter > offsetCount) {
                data.add(item);
            }
        }

        searchResponse.setData(data);

        return searchResponse;
    }

    private Map<Page, Double> relevance(HashSet<Page> generalPages, List<Index> searchIndex) {

        HashMap<Page, Integer> relevanceAbsolute = new HashMap<>();
        HashMap<Page, Double> relevanceRelativeNoSort = new HashMap<>();
        List<Integer> relevanceList = new ArrayList<>();

        for (Page page : generalPages) {
            int i = 0;
            for (Index index : searchIndex) {
                if (index.getPageId().equals(page)) {
                    i = i + index.getRang();
                }
            }
            relevanceList.add(i);
            relevanceAbsolute.put(page, i);
        }

        double maxRelevanceAbsolute = Collections.max(relevanceList);
        double relevance = 0.0;

        for (Page page : relevanceAbsolute.keySet()) {
            relevance = relevanceAbsolute.get(page) / maxRelevanceAbsolute;
            relevanceRelativeNoSort.put(page, relevance);
        }

        Map<Page, Double> relevanceRelative = relevanceRelativeNoSort.entrySet().stream().
                sorted(Map.Entry.<Page, Double>comparingByValue().reversed()).collect(Collectors.
                        toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        return relevanceRelative;
    }

    private HashSet<Page> sortedPages(HashSet<Lemma> searchLemma,
                                      List<Index> searchIndex, List<Page> searchPageList) {

        HashSet<Page> thirdSelectionList = new HashSet<Page>();
        HashSet<Page> firstSelectionList = new HashSet<Page>();
        HashSet<Page> secondSelectionList = new HashSet<Page>();

        for (Lemma lemma : searchLemma) {
            for (Index index : searchIndex) {
                if (index.getLemmaId().getLemma().equals(lemma.getLemma()) &&
                        searchPageList.contains(index.getPageId())) {
                    firstSelectionList.add(index.getPageId());
                }
            }

            if (thirdSelectionList.size() == 0) {
                thirdSelectionList = firstSelectionList;
                firstSelectionList = new HashSet<Page>();
            } else {
                for (Page page1 : firstSelectionList) {
                    if (thirdSelectionList.contains(page1.getPath())) {
                        secondSelectionList.add(page1);
                        thirdSelectionList = secondSelectionList;
                        firstSelectionList = new HashSet<Page>();
                        secondSelectionList = new HashSet<Page>();
                    }
                }
            }
        }

        return thirdSelectionList;
    }

    private void setSearchIndex(String query, List<Index> searchIndex,
                                HashSet<Lemma> searchLemma) throws IOException {

        HashMap<Index, Integer> indexMap = new HashMap<>();

        for (String word : LemmaFinder.getInstance().getLemmaSet(query)) {
            for (Index index : indexRepository.findAll()) {
                if (index.getLemmaId().getLemma().equals(word)) {
                    indexMap.put(index, index.getRang());
                    searchLemma.add(index.getLemmaId());
                }
            }
        }

        Map<Index, Integer> result = indexMap.entrySet().stream().
                sorted(Map.Entry.comparingByValue()).collect(Collectors.
                        toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        ArrayList<Index> arrayList = new ArrayList<>();


        for (Index index : result.keySet()) {
            arrayList.add(index);
        }

        Collections.reverse(arrayList);

        for (Index index : arrayList) {
            searchIndex.add(index);
        }
    }

    private void setSearchPageList(String site, List<Page> searchPageList) {
        boolean siteSearchString = true;

        if (site == null) {
            siteSearchString = false;
        }

        for (Page page : pageRepository.findAll()) {
            if (siteSearchString == true && page.getSiteId().getUrl().equals(site) && page.getCode() == 200) {
                searchPageList.add(page);
            } else if (siteSearchString == false && page.getCode() == 200) {
                searchPageList.add(page);
            }
        }
    }

    private String snippet(String inText, String query) {
        String text = clearText(inText);
        StringBuilder snippet = new StringBuilder();
        lemmaWordMap = new HashMap<>();
        List<String> tList = new ArrayList<>();
        List<String> qList = new ArrayList<>();

        for (String word : text.split("\\s+")) {
            word = word.replaceAll("[^А-Яа-я' ']", "");
            String lemma = word.toLowerCase(Locale.ROOT);
            tList.add(lemma);
            lemmaWordMap.put(lemma, word);
        }

        for (String word : query.split("\\s+")) {
            qList.add(word.toLowerCase(Locale.ROOT));
        }

        List<Integer> foundsWordsIndexes = new ArrayList<>();
        tList = createListFromText(tList, qList, foundsWordsIndexes);

        if (foundsWordsIndexes.isEmpty()) {
            if (inText.length() < 400) {
                return inText;
            }
            return null;
        }

        for (String tWord : tList) {
            snippet.append(lemmaWordMap.get(tWord)).append(" ");
        }

        return new String(snippet);
    }

    private String clearText(String text) {
        return text.replaceAll("[^А-Яа-яA-Za-z ]+", "");
    }


    private List<String> createListFromText(List<String> tList, List<String> qList,
                                            List<Integer> foundsWordsIndexes) {
        for (String qWord : qList) {
            if (!tList.contains(qWord)) {
                continue;
            }

            int indexOf = tList.indexOf(qWord);
            String wordInText = lemmaWordMap.get(qWord);
            lemmaWordMap.replace(qWord, wordInText, "<b>" + wordInText + "</b>");
            foundsWordsIndexes.add(indexOf);

            if (tList.size() <= 60) {
                continue;
            }

            int startSL = indexOf < 20 ? indexOf : indexOf - 20;
            int endSL = startSL;

            if (startSL + 60 <= tList.size()) {
                endSL = startSL + 60;
            } else if (startSL + 40 <= tList.size()) {
                endSL = startSL + 40;
            } else if (startSL + 20 <= tList.size()) {
                endSL = startSL + 20;
            }

            tList = tList.subList(startSL, endSL);
        }
        return tList;
    }
}
