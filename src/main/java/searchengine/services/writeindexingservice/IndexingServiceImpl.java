package searchengine.services.writeindexingservice;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import searchengine.config.SitesList;
import searchengine.dto.indexing.IndexPageResponse;
import searchengine.dto.indexing.IndexingResponse;
import searchengine.dto.indexing.StopIndexingResponse;
import searchengine.services.GetSiteCode;
import searchengine.services.IndexingService;
import searchengine.services.indexingsite.LinkExecutor;
import searchengine.model.*;
import searchengine.repositorys.IndexRepository;
import searchengine.repositorys.LemmaRepository;
import searchengine.repositorys.PageRepository;
import searchengine.repositorys.SiteRepository;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
@RequiredArgsConstructor
public class IndexingServiceImpl implements IndexingService {

    private final SiteRepository siteRepository;
    private final PageRepository pageRepository;
    private final LemmaRepository lemmaRepository;
    private final IndexRepository indexRepository;

    private String lastError = "";
    private StatusType statusType = StatusType.INDEXING;

    private GetSiteCode getSiteCode = new GetSiteCode();

    private final SitesList list;

    protected static Boolean stopIndexing = true;

    public IndexingResponse getStartIndexing() {

        IndexingResponse response = new IndexingResponse();

        if (stopIndexing.equals(false)) {

            response.setResult(false);
            response.setError("Индексация уже запущена");
            return response;

        } else {
            stopIndexing = false;
            deleteSiteData();
            clearWriteArrayList();

            for (int i = 0; i < list.getSites().size(); i++) {

                String url = list.getSites().get(i).getUrl();
                String name = list.getSites().get(i).getName();

                lastError = "";
                statusType = StatusType.INDEXING;
                Site site = new Site();
                site.setStatus(statusType);
                site.setName(name);
                site.setUrl(url);
                site.setLastError(lastError);
                site.setStatusTime(new Date());

                siteRepository.save(site);

                ThreadIndexing threadIndexing = new ThreadIndexing(site, siteRepository, pageRepository,
                        lemmaRepository, indexRepository);
                threadIndexing.start();
            }

            response.setResult(true);

            return response;
        }
    }

    public StopIndexingResponse getStopIndexing() {

        StopIndexingResponse response = new StopIndexingResponse();

        if (stopIndexing.equals(true)) {

            response.setResult(false);
            response.setError("Индексация не запущена");

            return response;
        } else {
            stopIndexing = true;
            response.setResult(true);

            return response;
        }
    }

    public IndexPageResponse getIndexPage(String url) {

        IndexPageResponse response = new IndexPageResponse();

        try {

            Site site;
            String[] split = url.split("/");
            String siteUrl = split[0] + "/" + split[1] + "/" + split[2];

            if (siteRepository.findAll().contains(siteRepository.findByUrl(siteUrl))) {
                site = siteRepository.findByUrl(siteUrl);
            } else {
                response.setResult(false);
                response.setError("Данная страница находится за пределами сайтов, " +
                        "указанных в конфигурационном файле\n");
                return response;
            }

            getIndexingPge(url, site);

        } catch (Exception e) {
            response.setResult(false);
            response.setError("Данная страница находится за пределами сайтов, " +
                    "указанных в конфигурационном файле\n");
            return response;
        }
        response.setResult(true);
        return response;
    }

    public static void clearWriteArrayList() {
        LinkExecutor.WRITE_ARRAY_LIST = new CopyOnWriteArrayList<>();
    }

    private void getIndexingPge(String url, Site site) throws IOException {

        if (pageRepository.findAll().contains(pageRepository.findByPath(url))) {
            Page page = pageRepository.findByPath(url);
            page.setContent(getSiteCode.getCode(url));
            URL urlSite = new URL(url);
            HttpURLConnection http = (HttpURLConnection) urlSite.openConnection();
            int statusCode = http.getResponseCode();
            page.setCode(statusCode);
            pageRepository.save(page);

            for (Index indx : indexRepository.findAll()) {
                if (indx.getPageId().equals(page)) {
                    Lemma lemma = lemmaRepository.findById(indx.getLemmaId().getId()).get();

                    lemma.setFrequency(lemma.getFrequency() - indx.getRang());
                    lemmaRepository.save(lemma);
                    indexRepository.delete(indx);
                }
            }

            LemmaAndIndexWriter writerLemmaAndIndex = new LemmaAndIndexWriter(lemmaRepository, indexRepository);
            writerLemmaAndIndex.lemmaAndIndexWriter(page.getSiteId(), page, page.getContent());

        } else {

            URL urlSite = new URL(url);
            HttpURLConnection http = (HttpURLConnection) urlSite.openConnection();
            int statusCode = http.getResponseCode();

            Page page = new Page();
            page.setContent(getSiteCode.getCode(url));
            page.setCode(statusCode);
            page.setPath(url.trim());
            page.setSiteId(site);
            pageRepository.save(page);

            LemmaAndIndexWriter writerLemmaAndIndex = new LemmaAndIndexWriter(lemmaRepository, indexRepository);
            writerLemmaAndIndex.lemmaAndIndexWriter(page.getSiteId(), page, page.getContent());
        }
    }

    private void deleteSiteData() {

        indexRepository.deleteAll();
        lemmaRepository.deleteAll();
        pageRepository.deleteAll();
        siteRepository.deleteAll();

    }
}
