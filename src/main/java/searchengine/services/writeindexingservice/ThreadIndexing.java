package searchengine.services.writeindexingservice;

import searchengine.model.*;
import searchengine.repositorys.IndexRepository;
import searchengine.repositorys.LemmaRepository;
import searchengine.repositorys.PageRepository;
import searchengine.repositorys.SiteRepository;

public class ThreadIndexing extends Thread {

    private Site site;
    private SiteRepository siteRepository;
    private PageRepository pageRepository;
    private LemmaRepository lemmaRepository;
    private IndexRepository indexRepository;

    public ThreadIndexing(Site site, SiteRepository siteRepository, PageRepository pageRepositor,
                          LemmaRepository lemmaRepository, IndexRepository indexRepository) {
        this.site = site;
        this.siteRepository = siteRepository;
        this.pageRepository = pageRepositor;
        this.lemmaRepository = lemmaRepository;
        this.indexRepository = indexRepository;
    }

    @Override
    public void run() {

        SitesWriter writeSites = new SitesWriter(pageRepository, siteRepository, lemmaRepository,
                indexRepository).
                sitesWriter(site);
    }
}
