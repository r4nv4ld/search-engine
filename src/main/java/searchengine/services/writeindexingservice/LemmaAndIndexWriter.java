package searchengine.services.writeindexingservice;

import searchengine.model.Index;
import searchengine.model.Lemma;
import searchengine.model.Page;
import searchengine.model.Site;
import searchengine.repositorys.IndexRepository;
import searchengine.repositorys.LemmaRepository;
import searchengine.services.indexingsite.LemmaFinder;

import java.util.HashMap;

public class LemmaAndIndexWriter {

    private LemmaRepository lemmaRepository;
    private IndexRepository indexRepository;

    public LemmaAndIndexWriter(LemmaRepository lemmaRepository, IndexRepository indexRepository) {
        this.lemmaRepository = lemmaRepository;
        this.indexRepository = indexRepository;
    }

    protected LemmaAndIndexWriter lemmaAndIndexWriter(Site siteId, Page pageId, String content) {

        try {

            HashMap lemmas = (HashMap) LemmaFinder.getInstance().collectLemmas(content);

            for (Object kye : lemmas.keySet()) {

                String word = (String) kye;

                Lemma lemma = lemmaRepository.findByLemmaAndSiteId(word, siteId);
                if (lemma == null) {
                    lemma = new Lemma();
                    lemma.setLemma(word);
                    lemma.setFrequency((Integer) lemmas.get(kye));
                    lemma.setSiteId(siteId);
                } else {
                    lemma.setFrequency(lemma.getFrequency() + (Integer) lemmas.get(kye));
                }
                lemmaRepository.save(lemma);

                Index index = new Index();
                index.setLemmaId(lemma);
                index.setPageId(pageId);
                index.setRang((Integer) lemmas.get(kye));
                indexRepository.save(index);

            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return null;
    }
}
