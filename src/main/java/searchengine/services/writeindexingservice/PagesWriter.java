package searchengine.services.writeindexingservice;

import searchengine.model.Page;
import searchengine.model.Site;
import searchengine.model.StatusType;
import searchengine.repositorys.IndexRepository;
import searchengine.repositorys.LemmaRepository;
import searchengine.repositorys.PageRepository;
import searchengine.services.GetSiteCode;
import searchengine.services.indexingsite.StartIndexingSites;

import java.net.HttpURLConnection;
import java.net.URL;

public class PagesWriter {

    private Site site;
    private PageRepository pageRepository;
    private LemmaRepository lemmaRepository;
    private IndexRepository indexRepository;

    private StartIndexingSites startIndexingSites = new StartIndexingSites();
    private GetSiteCode getSiteCode = new GetSiteCode();
    protected static String[] indexing;

    public PagesWriter(Site site, PageRepository pageRepository, LemmaRepository lemmaRepository, IndexRepository indexRepository) {
        this.site = site;
        this.pageRepository = pageRepository;
        this.lemmaRepository = lemmaRepository;
        this.indexRepository = indexRepository;
    }

    protected String[] pagesWriter(Site siteId) {

        String url = siteId.getUrl();
        indexing = startIndexingSites.start(url);
        for (String path : indexing) {
            if (IndexingServiceImpl.stopIndexing.equals(true)) {
                site.setLastError("Индексация остановлена пользователем");
                site.setStatus(StatusType.FAILED);

                return null;
            }
            try {

                URL urlSite = new URL(path);
                HttpURLConnection http = (HttpURLConnection) urlSite.openConnection();
                int statusCode = http.getResponseCode();

                Page page = new Page();
                page.setContent(getSiteCode.getCode(path));
                page.setCode(statusCode);
                page.setPath(path.trim());
                page.setSiteId(siteId);

                if (!url.trim().equals(path.trim())) {
                    pageRepository.save(page);
                    LemmaAndIndexWriter writerLemmaAndIndex = new LemmaAndIndexWriter(lemmaRepository, indexRepository).
                            lemmaAndIndexWriter(page.getSiteId(), page, page.getContent());
                }

            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
        return indexing;
    }
}
