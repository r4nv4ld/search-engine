package searchengine.services.writeindexingservice;

import searchengine.model.Site;
import searchengine.model.StatusType;
import searchengine.repositorys.IndexRepository;
import searchengine.repositorys.LemmaRepository;
import searchengine.repositorys.PageRepository;
import searchengine.repositorys.SiteRepository;

import java.util.Date;

public class SitesWriter {

    private PageRepository pageRepository;
    private SiteRepository siteRepository;
    private LemmaRepository lemmaRepository;
    private IndexRepository indexRepository;

    protected static String[] indexing;
    private String lastError = "";
    private StatusType statusType = StatusType.INDEXING;

    public SitesWriter(PageRepository pageRepository, SiteRepository siteRepository, LemmaRepository lemmaRepository,
                       IndexRepository indexRepository) {
        this.pageRepository = pageRepository;
        this.siteRepository = siteRepository;
        this.lemmaRepository = lemmaRepository;
        this.indexRepository = indexRepository;
    }

    protected SitesWriter sitesWriter(Site site) {

        PagesWriter writerPages = new PagesWriter(site, pageRepository, lemmaRepository, indexRepository);
        indexing = writerPages.pagesWriter(site);

        if (PagesWriter.indexing.length == 0) {
            lastError = "ошибка индексации";
            statusType = StatusType.FAILED;
        } else if (IndexingServiceImpl.stopIndexing.equals(true)) {
            lastError = "Индексация остановлена пользователем";
            statusType = StatusType.FAILED;
        } else {
            lastError = "";
            statusType = StatusType.INDEXED;
        }

        site.setStatus(statusType);
        site.setLastError(lastError);
        site.setStatusTime(new Date());

        siteRepository.save(site);

        return null;
    }
}
