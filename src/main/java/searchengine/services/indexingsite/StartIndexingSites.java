package searchengine.services.indexingsite;

import java.util.concurrent.ForkJoinPool;

public class StartIndexingSites {

    private static String[] listSites;
    private static String url;
    private static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    public String[] start(String site) {

        try {
            url = site;
        } catch (Exception e) {
            e.printStackTrace();
        }

        int numberOfThreads = 0;
        try {
            numberOfThreads = Runtime.getRuntime().availableProcessors();
        } catch (Exception e) {
            e.printStackTrace();
        }

        LinkExecutor linkExecutor = new LinkExecutor(url);
        String siteMap;
        if (numberOfThreads == 0) siteMap = new ForkJoinPool(NUMBER_OF_CORES).invoke(linkExecutor);
        else siteMap = new ForkJoinPool(numberOfThreads).invoke(linkExecutor);

        listSites = siteMap.split("\n");
        linkExecutor.compute();

        if (listSites.length <= 1) {
            return new String[0];
        }
        return listSites;
    }
}