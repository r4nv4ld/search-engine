package searchengine.services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class GetSiteCode {
    public String getCode(String url) {

        String title = "";
        try {
            Document doc = Jsoup.connect(url).get();
            title = doc.outerHtml();

        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return title;
    }
}
